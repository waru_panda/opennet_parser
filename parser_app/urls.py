from django.conf.urls import url, patterns, include
from parser_app import views

urlpatterns = patterns('',
                       url(r'^$', views.Main.as_view(), name='main')
                       )