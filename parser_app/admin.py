from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin
from parser_app import models


@admin.register(models.NewsItem)
class NewsItem(ModelAdmin):
    model = models.NewsItem
    list_display = ('datetime', 'topic', 'preview')
