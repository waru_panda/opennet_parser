import urllib.request
from bs4 import BeautifulSoup
from django.shortcuts import render
from django.views.generic import TemplateView


# Create your views here.
from parser_app import models


class Main(TemplateView):
    template_name = 'main.html'


class Parser(object):
    # словарь с идентификаторами нужных блоков в разметке сайта, url сайта и количество страниц для парсинга
    def __init__(self, base_url, classes, pages):
        self.classes = classes
        self.base_url = base_url
        self.pages = pages

    # выдает приведенную в читабельный вид таблицу с новостями
    def get_table(self, url):
        response = urllib.request.urlopen(url)
        soup = BeautifulSoup(response, 'lxml')
        table = soup.find('table', class_="%s" % self.classes.get('table_class'))
        return table

    # следующие методы вытаскивают списки необходимых значений со страницы - список дат, топиков итд.
    # аргументы в find_all записываются в таком виде для того чтобы значение выдавалось в '',
    # иначе бьютифулсуп выдаст None
    def get_datetime(self, table):
        soup_datetime = table.find_all('td', class_="%s" % self.classes.get('datetime_class'))
        datetime_list = [date.text for date in soup_datetime]
        return datetime_list

    def get_topic(self, table):
        soup_topic = table.find_all('td', class_="%s" % self.classes.get('topic_class'))
        topic_list = [topic.text for topic in soup_topic]
        return topic_list

    def get_preview(self, table):
        soup_preview = table.find_all('td', class_="%s" % self.classes.get('preview_class'))
        preview_list = [preview.text for preview in soup_preview]
        return preview_list

    def get_url(self, table):
        soup_url = table.find_all('a', class_="%s" % self.classes.get('topic_class'))
        url_list = [self.base_url + url.get('href') for url in soup_url]
        return url_list

    def pagination(self, table, pages):
        base_url_list = [self.base_url + page for page in range(0, pages)]
        datetime_list, topic_list, preview_list, url_list = []
        for url in base_url_list:
            table = self.get_table(url)
            datetime_list += self.get_datetime(table)
            topic_list += self.get_topic(table)
            preview_list += self.get_preview(table)
            datetime_list += self.get_datetime(table)


class Opennet(Parser):
    base_url = 'https://www.opennet.ru/news/'
    classes = {'table_class': 'ttxt', 'datetime_class': 'tdate', 'topic_class': 'title2', 'preview_class': 'chtext2'}

    def get_topic(self, table):
        soup_topic = table.find_all('a', class_="%s" % self.classes.get('topic_class'))
        topic_list = [topic.text for topic in soup_topic]
        return topic_list

    def get_url(self, table):
        soup_url = table.find_all('a', class_="%s" % self.classes.get('topic_class'))
        url_list = [self.base_url[:21] + url.get('href') for url in soup_url]
        return url_list

    def pagination(self, table, pages):
        url_list = ['http://www.opennet.ru/opennews/index.shtml?skip={}&news=open&template=0'.format(page * 15)
                    for page in range(0, pages)]
