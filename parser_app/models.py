# coding: utf-8

from django.db import models


# Create your models here.

class NewsItem(models.Model):
    datetime = models.DateTimeField('Дата публикации', blank=True, null=True)
    topic = models.CharField('Тема', blank=True, null=True, max_length=200)
    preview = models.CharField('Превью', max_length=2000)
    url = models.URLField('Ссылка на оригинал')
